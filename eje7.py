# Author: "Steven   Velez"
# Email: "steven.velez@unl.edu.ec"

# "Caliﬁcaciones usando una función llamada calcula_calificacion, que reciba una
# puntuación como parámetro y devuelva una caliﬁcación como cadena

# definida la funcion:

def calcula_calificacion(e):
    if e >= 0  and e  <=  1.0:
        if e >=  0.9:
            print("Sobresaliente")
        elif e >=  0.8:
            print("Notable")
        elif e >=  0.7:
            print("Bien")
        elif e <=  0.6:
            print("Insuficiente")
    else:
        print("Fuera de rango")


try:
    puntuacion  =   float(input("Inserte puntuacion:\n"))
    calcula_calificacion(puntuacion)
except:
    print("Puntuacion incorrecta")
