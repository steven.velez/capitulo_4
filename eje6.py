# "Ejercicios 4.14"
# Author: "Steven F. Velez "
# Email: "steven.velez@unl.edu.ec

# "crea una función llamada calculo_salario que reciba dos parámetros (horas y tarifa)"
# "definimos la funcion y parametros"


def calculo_salario (h,p):
    if h <= 40:
        total  =   h   *   p
        print("La cantidad que recibida es de ", total, " dolares")
    elif h >= 41:
        h   -=  40
        total   =   40   *   p
        incre   =   1.5 * (h * p)
        total   +=  incre
        print("La cantidad que recibida es de ", total, " dolares")
    else:
        print("La cantidad ingresada es incorrecta")


try:
    saldo   =   float(input("Ingrese el numero de horas trabajadas:\n"))
    tarifa  =   float(input("Ingrese el monto por hora:\n"))
    calculo_salario(saldo,tarifa)

except:
    print("La cantidad ingresada es incorrecta")
